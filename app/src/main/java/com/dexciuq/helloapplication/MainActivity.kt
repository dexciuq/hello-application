package com.dexciuq.helloapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dexciuq.helloapplication.databinding.ActivityMainBinding

private const val NAME = "Dinmukhammed"

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.greeting.text = getString(R.string.greeting, NAME)
    }
}